ShopifyApp.configure do |config|
  config.application_name = "My Shopify App"
  config.api_key = "1311d3a7502df96fa4c0273eff4fd58e"
  config.secret = "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"
  config.old_secret = "<old_secret>"
  config.scope = "read_orders, read_products, write_shipping, write_fulfillments" # Consult this page for more scope options:
                                 # https://help.shopify.com/en/api/getting-started/authentication/oauth/scopes
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
end
